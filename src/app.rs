use std::{
	env,
	io::{self},
	path::PathBuf,
};

use crate::{
	build_command::BuildCommand, file_finder::FileFinder, logger::Logger,
	uninstaller_builder::UninstallerBuilder, package_installer::PackageInstaller,
};

const APP_NAME: &str = "makemgr";
const SYS_LOG_PATH: &str = "/var/log";
const SYS_SAVE_PATH: &str = "/var/lib";

pub struct App {
	file_finder: FileFinder,
	build_cmd: BuildCommand,
	logger: Logger,
	uninstaller: UninstallerBuilder,
	installer: PackageInstaller,
}

impl App {
	pub fn new(package_name: &str, make_target: Option<String>) -> Result<Self, io::Error> {
		let build_dir = env::temp_dir().join(APP_NAME).join(package_name);

		if build_dir.exists() {
			std::fs::remove_dir_all(&build_dir)?;
		}

		let build_command = BuildCommand::new("make")
			.add_argument({
				if let Some(target) = make_target.as_ref() {
					target
				} else {
					"install"
				}
			})
			.add_argument(&("DESTDIR=".to_owned() + &build_dir.to_string_lossy()));

		let log_path = PathBuf::from(SYS_LOG_PATH)
			.join(APP_NAME)
			.join(package_name);

		let uninstaller = UninstallerBuilder::new(
			PathBuf::from(SYS_SAVE_PATH)
				.join(APP_NAME)
				.join(&package_name),
			package_name,
		)?;

		let logger = Logger::new(log_path)?;
		return Ok(Self {
			file_finder: FileFinder::new(&build_dir),
			build_cmd: build_command,
			logger: logger,
			uninstaller: uninstaller,
			installer: PackageInstaller::new(&build_dir),
		});
	}

	pub fn run(&mut self) -> Result<bool, io::Error> {
		let success = self.build_cmd.execute()?;
		self.logger.log_command_output(
			self.build_cmd
				.get_stdout()
				.expect("Failed to get command stdout."),
			self.build_cmd
				.get_stderr()
				.expect("Failed to get command stderr."),
		)?;

		if success == true {
			self.file_finder.find(None)?;
			self.uninstaller
				.build(self.file_finder.get_file_str_list().as_slice())?;
			self.installer.install(self.file_finder.get_file_path_list())?;
		}

		return Ok(success);
	}
}
