use std::fs;
use std::io;
use std::path::Path;
use std::path::PathBuf;

pub struct FileFinder {
	dir_path: PathBuf,
	path_list: Vec<PathBuf>,
}

impl FileFinder {
	pub fn new(dir: &Path) -> Self {
		return Self {
			dir_path: dir.to_path_buf(),
			path_list: Vec::new(),
		};
	}

	pub fn find(&mut self, path: Option<&PathBuf>) -> Result<(), io::Error> {
		let dir = fs::read_dir({
			if let Some(dir) = path {
				dir
			} else {
				&self.dir_path
			}
		})?;

		for entry in dir {
			let entry = entry?;
			let entry_metadata = entry.metadata()?;
			if entry_metadata.is_dir() {
				self.find(Some(&entry.path()))?;
			} else {
				let path = entry.path();
				self.path_list.push(path);
			}
		}

		return Ok(());
	}

	pub fn get_file_str_list(&self) -> Vec<String> {
		return self
			.path_list
			.iter()
			.map(|path| {
				Path::new("/")
					.join(
						path.strip_prefix(&self.dir_path).expect(
							format!(
								"Could not strip prefix: {}",
								self.dir_path.as_path().to_string_lossy()
							)
							.as_str(),
						),
					)
					.to_string_lossy()
					.to_string() + "\n"
			})
			.collect();
	}

	pub fn get_file_path_list(&self) -> &[PathBuf] {
		return self.path_list.as_slice();
	}
}
