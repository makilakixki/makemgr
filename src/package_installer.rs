use std::{
	fs, io,
	path::{Path, PathBuf},
};

pub struct PackageInstaller {
	build_dir: PathBuf,
}

impl PackageInstaller {
	pub fn new(build_dir: &Path) -> Self {
		return Self {
			build_dir: build_dir.to_path_buf(),
		};
	}

	pub fn install(&self, path_list: &[PathBuf]) -> Result<(), io::Error> {
		for src_path in path_list {
			let dest_path = Path::new("/").join({
				src_path.strip_prefix(&self.build_dir).expect(
					format!(
						"Could not strip prefix: {}",
						self.build_dir.as_path().to_string_lossy()
					)
					.as_str(),
				)
			});
			fs::create_dir_all({
				if let Some(parent) = dest_path.parent() {
					parent
				} else {
					return Err(io::Error::new(
						io::ErrorKind::Other,
						format!("'{}' Invalid parent directory.", dest_path.display()),
					));
				}
			})?;

			fs::copy(&src_path, &dest_path)?;
			println!(
				"Installing: {} --> {}",
				src_path.display(),
				dest_path.display()
			);
		}
		return Ok(());
	}
}
