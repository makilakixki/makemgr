use std::fs::{self, File};
use std::io::{self, Write};
use std::path::PathBuf;

pub struct Logger {
	stdout_file: File,
	stderr_file: File,
}

impl Logger {
	pub fn new(log_path: PathBuf) -> Result<Self, io::Error> {
		fs::create_dir_all(&log_path)?;
		let stdout = File::create(log_path.join("stdout.log"))?;
		let stderr = File::create(log_path.join("stderr.log"))?;
		return Ok(Self {
			stdout_file: stdout,
			stderr_file: stderr,
		});
	}

	pub fn log_command_output(&mut self, stdout: &str, stderr: &str) -> Result<(), io::Error> {
		if stdout.is_empty() == false {
			self.stdout_file.write_all(stdout.as_bytes())?;
		}

		if stderr.is_empty() == false {
			self.stderr_file.write_all(stderr.as_bytes())?;
		}

		return Ok(());
	}
}
