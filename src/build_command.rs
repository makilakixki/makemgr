use std::io;
use std::process::Command;
use std::str::{self, Utf8Error};

pub struct BuildCommand {
	command: Command,
	stdout: Vec<u8>,
	stderr: Vec<u8>,
}

impl BuildCommand {
	pub fn new(command: &str) -> Self {
		return Self {
			command: Command::new(command),
			stdout: Vec::new(),
			stderr: Vec::new(),
		};
	}

	pub fn execute(&mut self) -> Result<bool, io::Error> {
		let output = self.command.output()?;

		if output.stdout.is_empty() == false {
			self.stdout = output.stdout;
		}

		if output.stderr.is_empty() == false {
			self.stderr = output.stderr;
		}

		return Ok(output.status.success());
	}

	pub fn add_argument(mut self, arg: &str) -> Self {
		self.command.arg(arg);
		return self;
	}

	pub fn get_stdout(&self) -> Result<&str, Utf8Error> {
		return std::str::from_utf8(self.stdout.as_slice());
	}

	pub fn get_stderr(&self) -> Result<&str, Utf8Error> {
		return std::str::from_utf8(self.stderr.as_slice());
	}
}
