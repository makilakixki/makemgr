mod app;
mod build_command;
mod file_finder;
mod logger;
mod uninstaller_builder;
mod package_installer;

use app::App;

use clap::Parser;

#[derive(Parser)]
#[clap(author, version, about)]
struct Args {
	/// Make install target. Default is 'install'.
	target: Option<String>,

	/// Package name.
	#[clap(short, long)]
	name: String,
}

fn main() -> Result<(), String> {
	let args = Args::parse();

	let mut app = match App::new(&args.name, args.target) {
		Ok(app) => app,
		Err(err) => return Err(err.to_string()),
	};

	let success = match app.run() {
		Ok(success) => success,
		Err(err) => return Err(err.to_string()),
	};

	if success == false {
		return Err(format!(
			"Something went wrong. Please check the logs at /var/log/makemgr/{package}",
			package = &args.name
		));
	}

	return Ok(());
}
