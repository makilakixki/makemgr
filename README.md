# makemgr
A program that keeps track of package files installed with `make install`.

## Usage
`user@host:~/path/to/makefile$ makemgr -n "Package name" <OPTIONAL INSTALL TARGET>`

The optional install target is, by default, `install`.

This command will create an uninstaller script in `/var/lib/makemgr/<package_name>/<package_name>-uninstaller.sh`, as well as a list of installed files in `/var/lib/makemgr/<package_name>/<package_name>-installed_files`.

## Description
This program installs the package in a temporary directory (i.e `/tmp/makemgr/<package_name>`), tracks all the installed files to `/var/lib/makemgr/<package_name>/<package_name>-installed_files`, creates an uninstaller in `/var/lib/makemgr/<package_name>/<package_name>-uninstaller.sh`, and then copies the files to their real paths.

## Risks
You will probably need to run this program with root privileges, so be careful.